# Password Safe to KeepassXC database migrator

This tool migrates [Password Safe](https://pwsafe.org/)'s "Plain text (tab separated)" format to a CSV that's easy to import in [KeepassXC](https://keepassxc.org/).

The main issue is that Password Safe combines the Group and Title in a single field, but since I was already writing a script to fix that I used that opportunity to output a file that can be imported as-is without needed to change a bunch of things in the import options.

I used it only once so it's not immensely battle tested but hopefully it can be useful to someone else out there!

Usage: `ruby migrate_database.rb path_to_password_safe_export.txt`

It will output another file prefixed with `keepassxc-` and leave the original intact.
