# frozen_string_literal: true

require 'csv'

pwsafe_db = ARGV[0]

begin
  first_row = true
  CSV.open("keepassxc-#{pwsafe_db}", 'w') do |csv|
    CSV.foreach(pwsafe_db, col_sep: "\t") do |row|
      if first_row
        # We don't need the titles
        first_row = false
        next
      end

      # Password Safe's first column is a Group.Title format. KeepassXC expects two columns
      group_separator_position = row[0].index('.')

      new_row = [
        (row[0][0..group_separator_position - 1] unless group_separator_position.nil?), # Group
        row[0][(group_separator_position.nil? ? 0 : group_separator_position + 1)..], # Title
        row[1], # Username
        row[2], # Password
        row[3], # URL
        row[20], # Notes
        nil, # TOTP
        nil, # Icon
        row[6], # Last Modified
        row[5] # Created
      ].map { |col| col&.gsub(/"/, '\\"') }

      csv << new_row
    end
  end
rescue CSV::MalformedCSVError => e
  warn(e.to_s)
  warn("Password Safe doesn't escape all double quotes correctly, you might need to manually correct the line in the message above.")
  warn('Quotes are escaped by doubling them, "", not \".')

  exit 1
end
